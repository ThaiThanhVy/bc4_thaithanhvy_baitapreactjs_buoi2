import React, { Component } from 'react';
import dataGlasses from '../Data/dataGlasses.json'

export default class Baitapthukinh extends Component {

    state = {
        glassesCurrent: {}
        ,
    }



    renderGlassList = () => {
        return dataGlasses.map((glassItem, index) => {
            return <img onClick={() => { this.changeClassess(glassItem) }} style={{ width: '100px', cursor: 'pointer', border: '1px solid black' }} key={index} src={glassItem.url} />
        });
    }

    // renderRaManHinh
    changeClassess = (newGlasses) => {
        this.setState({
            glassesCurrent: newGlasses
        })
    }

    render() {
        const keyFrame = `@keyframes animateGlasses${Date.now()} {
            from {
                width: 0;
            } to {
                width: 132px;
            }
        }`

        const styleGlasses = {
            width: '132px',
            top: '74px',
            left: '59px',
            opacity: '0.7',
            position: 'absolute',
            animation: `animateGlasses${Date.now()} 1s`
        }

        const img = {
            marginTop: '262px',
            display: 'flex',
            justifyContent: 'center',
            gap: '20px',
            height: '53px',
        }

        const pContent = {
            width: '250px',
            textAlign: 'left',
            top: '218px',
            paddingLeft: '10px',
            backgroundColor: 'rgba(255 , 127 , 0 ,.5)',
            height: '87px',
        }


        return (
            <div style={{ background: 'url(../glassesImage/background.jpg)', backgroundSize: 'cover', minHeight: '800px', backgroundRepeat: 'no-repeat' }}>
                <style>{keyFrame}</style>
                <div style={{ backgroundColor: 'rgba(0,0,0,.3)', minHeight: '800px' }}>
                    <h2 style={{ backgroundColor: 'rgba(148,0,211,.1)' }} className="text-light text-center py-5" >Try Glasses App Online</h2>
                    <div className="container">
                        <div className="row" style={{ marginLeft: '431px' }}>
                            <div style={{ position: 'relative' }}>
                                <img style={{ width: '250px', position: 'absolute' }} src="../glassesImage/model.jpg" alt="model.jpg" />
                                <div className="w-100" />
                                <img style={styleGlasses} src={this.state.glassesCurrent.url} alt="" />
                            </div>
                            <div className='position-relative' style={pContent}>
                                <span style={{ color: 'purple' }} className='font-weight-bold'>{this.state.glassesCurrent.name}
                                    <span style={{ marginLeft: '84px' }}>{this.state.glassesCurrent.price}</span>
                                </span>
                                <p style={{ fontSize: '13px' }}>{this.state.glassesCurrent.desc}</p>
                            </div>
                        </div>
                        <div className='bg-light container' style={img}>
                            {this.renderGlassList()}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


